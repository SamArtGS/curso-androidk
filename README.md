# Intersemestral Android 2019-2

```shell
mayo de 2019

Versiones de tecnologías
Android studio 3.4.1
Kotlin v1.3
```
*!Bienvenido a curso de android con Kotlin!* En este repositorio encontrarás todo el material necesario para un completo aprendizaje; presentaciones, códigos de ejemplos, tareas y ligas de intéres.

Para tomar este curso debes repasar como utilizar la herramienta de git para lo cual puedes leer esta [guía](notas/workflow.md).

La forma de **evaluar** se puede encontrar en la [primera presentación del curso](), aún así, aquí va de nuevo

* Proyecto 60 %

* Tareas 40 %

* Asistencia 10% 
  * *Para tener derecho a constancia se requiere del 85% de asistencia*

Para cualquier duda o aclaración por favor al corrreo electrónico rhodfra.proteco@gmail.com 

